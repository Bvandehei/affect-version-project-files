import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class RQ3 {

   public static void main(String[] args) throws IOException, InterruptedException {
      String csvSplitBy = ",";

      String title = "ProjectKey,BugID,BugOrder,VersionID,VersionName,VersionIndex,Simple,ProportionColdStart1,ProportionColdStart0.5,ProportionColdStart2,ProportionIncrement1,ProportionIncrement0.5,ProportionIncrement2,ProportionMovingWindow1%,ProportionMovingWindow5%,ProportionMovingWindow10%,U-SZZ,B-SZZ,RA-SZZ,ProportionColdStart1+,ProportionColdStart0.5+,ProportionColdStart2+,ProportionIncrement1+,ProportionIncrement0.5+,ProportionIncrement2+,ProportionMovingWindow1%+,ProportionMovingWindow5%+,ProportionMovingWindow10%+,U-SZZ+,B-SZZ+,RA-SZZ+,Actual";
      String [] titleSplit = title.split(csvSplitBy);

      try (BufferedReader br = new BufferedReader(new FileReader("../RQ1/TopProjects.csv"))) {
         String lineProjects = br.readLine();
         while ( (lineProjects = br.readLine()) != null ) {

           String[] token = lineProjects.split(csvSplitBy);
           String project = token[0];
           String path =  "";
           System.out.println(token[0]);
           for (Integer column = 6; column <= 31; column++) {
              //Get first AV of each Bug from MethodsResults
              HashMap<String,Integer> bugToFirstAV = new HashMap<String, Integer>();
				      try (BufferedReader brMethods = new BufferedReader(new FileReader("../RQ2/MethodsResults.csv"))) {
		            String linefrommethods = brMethods.readLine();
		            String[] tokenmethods = linefrommethods.split(csvSplitBy);
		            String current = "";
		            Integer av = -1;
		            while ( (linefrommethods = brMethods.readLine()) != null) {
		               tokenmethods = linefrommethods.split(csvSplitBy);
		               if (tokenmethods[0].equals(project)) {
		                  if (tokenmethods[1].equals(current) && tokenmethods[column].equals("Yes") && (Integer.parseInt(tokenmethods[5])<av || av<0)) {
		                     av = Integer.parseInt(tokenmethods[5]);
		                  } else if (!tokenmethods[1].equals(current)) {
                        if (!current.equals("")) {
                          bugToFirstAV.put(current, av);
                        }
                        current = tokenmethods[1];
                        av = -1;
                        if (tokenmethods[column].equals("Yes") && (Integer.parseInt(tokenmethods[5])<av || av<0))
												   av = Integer.parseInt(tokenmethods[5]);
                      }
		               }
		            }
				      } catch (IOException e) {
				         e.printStackTrace();
				      }

              //get version Info
              HashMap<Integer,String> versionIndexToDate = new HashMap<Integer,String>();
              HashMap<Integer,ArrayList<String>> versionIndexToBuggyFiles = new HashMap<Integer,ArrayList<String>>();
				      try (BufferedReader brV = new BufferedReader(new FileReader("../VersionInfo/"+project+"VersionInfo.csv"))) {
				         String linefromver = brV.readLine();
				         while ( (linefromver = brV.readLine()) != null) {
				            String[] tokenver = linefromver.split(csvSplitBy);
				            Integer indexver = Integer.parseInt(tokenver[0]);
				            versionIndexToDate.put(indexver, tokenver[3]);
				            versionIndexToBuggyFiles.put(indexver,new ArrayList<String>());
				         }
				      } catch (IOException e) {
				         e.printStackTrace();
				      }

              //get buggy files in versions
				      try (BufferedReader brBI = new BufferedReader(new FileReader("../BugInfoFiles/Ordered"+project+"BugInfo.csv"))) {
				         String linefromcsv = brBI.readLine();
				         while ( (linefromcsv = brBI.readLine()) != null) {
				            String[] tokenBI = linefromcsv.split(csvSplitBy);
				            Integer firstAV = bugToFirstAV.get(tokenBI[0]);
				            Integer fixVersion = Integer.parseInt(tokenBI[5]);
				            if (firstAV == null || firstAV == -1)
				               firstAV = fixVersion;
				            String[] classes = tokenBI[2].split("\\|");
				            String git = tokenBI[9];
                    path = git;
				            for (Integer i = firstAV; i < fixVersion; i++) {
				               ArrayList<String> buggyFiles = versionIndexToBuggyFiles.get(i);
				               for (Integer j = 0; j < classes.length; j++){
				                  buggyFiles.add(classes[j].trim());
				               }
				               versionIndexToBuggyFiles.put(i, buggyFiles);
				            }
				         }
				      } catch (IOException e) {
				         e.printStackTrace();
				      }

				      try (BufferedReader brMetrics = new BufferedReader(new FileReader("../Metrics/MetricFiles/"+project+"_Metrics.csv"))) {
				         FileWriter fileWriter = null;
				         try {
								    fileWriter = new FileWriter(project+"_"+titleSplit[column]+"_Complete.csv");
				            String linefromcsv = brMetrics.readLine();
				            fileWriter.append(linefromcsv + ",Buggy\n");
				            while ( (linefromcsv = brMetrics.readLine()) != null) {
				               String[] tokenMetrics = linefromcsv.split(csvSplitBy);
				               String className = tokenMetrics[1] ;
				               Integer versionIndex = Integer.parseInt(tokenMetrics[0]);
				               String buggy = "No";
				               if ( versionIndexToBuggyFiles.get(versionIndex).contains(className)) {
				                  buggy = "Yes";
				               }
				               fileWriter.append(linefromcsv + "," + buggy + "\n");
				            }
				         } catch (Exception e) {
				            System.out.println("Error in csv writer");
				            e.printStackTrace();
				         } finally {
				            try {
				               fileWriter.flush();
				               fileWriter.close();
				            } catch (IOException e) {
				               System.out.println("Error while flushing/closing fileWriter !!!");
				               e.printStackTrace();
				            }
				         }
				      } catch (IOException e) {
				         e.printStackTrace();
				      }

           }
           System.out.println(path);
           String command2 = "rm -rf " + path;
           Process proc2 = Runtime.getRuntime().exec(command2);
           proc2.waitFor();

         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
