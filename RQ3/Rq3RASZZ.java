import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.math.*;

public class Rq3RASZZ {

   public static void main(String[] args) throws IOException, InterruptedException {
      String csvSplitBy = ",";
      String [] rafiles = new String[]{"_RA-SZZ_Complete.csv" , "_RA-SZZ+_Complete.csv"};
      try (BufferedReader br = new BufferedReader(new FileReader("../RQ1/TopProjects.csv"))) {
         String lineProjects = br.readLine();
         while ( (lineProjects = br.readLine()) != null ) {
           String[] token = lineProjects.split(csvSplitBy);
           String project = token[0];
           String path =  "";
           for (Integer c = 0; c < 2; c++) {
		         FileWriter fileWriter = null;
		         try {
						    fileWriter = new FileWriter(project+rafiles[c]);
					      try (BufferedReader brMethods = new BufferedReader(new FileReader("oldrq3files/"+ project  + rafiles[c] ))) {
					        try (BufferedReader brActual = new BufferedReader(new FileReader("Complete/" +project +"_U-SZZ_Complete.csv"))) {
				            String linefrommethods = brMethods.readLine();
				            String linefromactual = brActual.readLine();
                    fileWriter.append(linefrommethods + "\n");
				            while ( (linefrommethods = brMethods.readLine()) != null
	                      &&  (linefromactual = brActual.readLine()) != null) {
				               String[] tokenmethods = linefrommethods.split(csvSplitBy);
				               String[] tokenactual = linefromactual.split(csvSplitBy);
	                     if (!tokenmethods[1].equals(tokenactual[1])) {
	                       System.out.println("Mismatch in file name");
	                       System.exit(-1);
	                     }
                       if ( tokenmethods[1].length() > 5 && tokenmethods[1].substring(tokenmethods[1].length() - 5).equals(".java")) {
                         fileWriter.append(linefrommethods + "\n");
                       } else {
                         fileWriter.append(linefromactual + "\n");
                       }
				            }

					        } catch (IOException e) {
					           e.printStackTrace();
					        }
					      } catch (IOException e) {
					         e.printStackTrace();
					      }
		         } catch (Exception e) {
		            System.out.println("Error in csv writer");
		            e.printStackTrace();
		         } finally {
		            try {
		               fileWriter.flush();
		               fileWriter.close();
		            } catch (IOException e) {
		               System.out.println("Error while flushing/closing fileWriter !!!");
		               e.printStackTrace();
		            }
		         }
           }
         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
