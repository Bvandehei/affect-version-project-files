import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class GetProjectInfo {

   public static void main(String[] args) throws IOException {

      String csvSplitBy = ",";
      HashMap<String, String> QM = new HashMap<String,String>();
      try (BufferedReader brQM = new BufferedReader(new FileReader("OrderedQualityModel.csv"))) {
         brQM.readLine();
         String linefromQM;
         while ( (linefromQM = brQM.readLine()) != null) {
            String[] tokenQM = linefromQM.split(csvSplitBy);
            QM.put(tokenQM[0] , linefromQM);
         }
      } catch (Exception e) {
         e.printStackTrace();
      }

      try (BufferedReader br = new BufferedReader(new FileReader("TopProjects.csv"))) {
         FileWriter fileWriter = null;
			   try {
            fileWriter = null;
            String outname = "ProjectDetails.csv";

            fileWriter = new FileWriter(outname);
            fileWriter.append("Project Key,Repo,Number of Commits Analyzed,Number of Files in Last Release Analyzed,Number Files Across Releases Analyzed (num rows in metrics file),"
             + "Percent of Java Files Across Releases Analyzed,Total Releases,Releases Analyzed,C/L Bugs with No AV Problems,Total C/L Bugs,"
             + "Percent C/L Bugs without AV,Percent C/L Bugs with AV after Ticket Creation,Percent of C/L Bugs with a Problem\n");
            String linefromcsv;
            while ( (linefromcsv = br.readLine()) != null) {
               String[] token = linefromcsv.split(csvSplitBy);
               Long numCommits = 0L;

               String[] token2 = token[1].split("/");
               String path =  token2[token2.length - 1].substring(0, token2[token2.length - 1].length() - 4);
               String command = "git clone " + token[1];
               Process proc = Runtime.getRuntime().exec(command);
               proc.waitFor();

               String[] tokenQM = QM.get(token[0]).split(csvSplitBy);
               Integer totReleases = Integer.parseInt(tokenQM[2]);
               Integer lastReleaseAnalyzed = (int)Math.ceil((double) totReleases / 2.0);
               String lastCommit = "";
               String versionfilename = "../VersionInfo/"+ token[0] + "VersionInfo.csv";
               /*Get Versions info for each project*/
               try (BufferedReader brVersions = new BufferedReader(new FileReader(versionfilename))) {
                 brVersions.readLine();
                 String linefromversion;
                 while ( (linefromversion = brVersions.readLine()) != null) {
                   String[] tokenVersion = linefromversion.split(csvSplitBy);
                   if (Integer.parseInt(tokenVersion[0]) == lastReleaseAnalyzed) {
                     command = "git --git-dir ./" + path + "/.git " +
                       "log --until " + tokenVersion[3] + " --pretty=format:%H -1";
                     proc = Runtime.getRuntime().exec(command);
                     BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                     lastCommit = reader.readLine();
                     proc.waitFor();
                   }
                 }
               } catch (Exception e) {
                 e.printStackTrace();
               }
               command = "git --git-dir ./" + path + "/.git " +
                       "--work-tree ./" +path+ "/ checkout " + lastCommit;
               proc = Runtime.getRuntime().exec(command);
               proc.waitFor();

               command = "git --git-dir ./" + path + "/.git rev-list --all --count";
               proc = Runtime.getRuntime().exec(command);
               BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
               String line = reader.readLine();
               if(line != null) {
							   numCommits += Long.parseLong(line);
               }
               proc.waitFor();

               command = "rm -rf " + path;
               proc = Runtime.getRuntime().exec(command);
               proc.waitFor();
               Long filesAcross = 0L;
               Long javaFilesAcross = 0L;
               Long filesInLast = 0L;
               try (BufferedReader brMetrics = new BufferedReader(new FileReader("../Metrics/MetricFiles/" + token[0] + "_Metrics.csv"))) {
                 brMetrics.readLine();
                 String linefrommetrics;
                 while ( (linefrommetrics = brMetrics.readLine()) != null) {
                   String[] tokenMetrics = linefrommetrics.split(csvSplitBy);
                   filesAcross++;
                   if ( tokenMetrics[1].length() > 5 && tokenMetrics[1].substring(tokenMetrics[1].length() - 5).equals(".java")) {
                     javaFilesAcross++;
                   }
                   if (Integer.parseInt(tokenMetrics[0]) == lastReleaseAnalyzed){
                     filesInLast++;
                   }
                 }
               } catch (Exception e) {
                 e.printStackTrace();
               }
               Double percentJava = (double)javaFilesAcross/filesAcross;
System.out.println(percentJava);
               fileWriter.append(token[0]+ "," + token[1] + "," + numCommits.toString() + "," + filesInLast.toString() + ","
                + filesAcross.toString() + "," + percentJava.toString() + "," + totReleases.toString() + ","
                + lastReleaseAnalyzed.toString() + "," + tokenQM[1] + "," + tokenQM[3] + "," + tokenQM[5] + "," + tokenQM[7] + "," + tokenQM[9] + "\n");

            }
         } catch (Exception e) {
            System.out.println("Error in csv writer");
            e.printStackTrace();
         } finally {
            try {
               fileWriter.flush();
               fileWriter.close();
            } catch (IOException e) {
               System.out.println("Error while flushing/closing fileWriter !!!");
               e.printStackTrace();
            }
         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
