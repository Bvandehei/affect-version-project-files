import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;



public class ComputeMetricsFix {

   public static void main(String[] args) throws IOException, InterruptedException {
      String csvSplitBy = ",";
      Integer x = 0;
      try (BufferedReader br = new BufferedReader(new FileReader("../RQ1/TopProjects.csv"))) {
         String lineProjects ;
         while ( (lineProjects = br.readLine()) != null ) {
           x = 0;
           String[] token = lineProjects.split(csvSplitBy);
           String project = token[0];
           String path =  "";
           System.out.println(token[0]);

				      try (BufferedReader brMetrics = new BufferedReader(new FileReader("oldMetricFiles/"+project+"_Metrics.csv"))) {
				         FileWriter fileWriter = null;
				         try {
								    fileWriter = new FileWriter(project+"_Metrics.csv");
				            String linefrommetrics = brMetrics.readLine();
                    fileWriter.append(linefrommetrics +"\n");

                    HashMap<String, Integer> versionclassBugFixes = new HashMap<String,Integer>();
                    try (BufferedReader brBI = new BufferedReader(new FileReader("../BugInfoFiles/"+project+"BugInfo.csv"))) {
                       String linefrombug= brBI.readLine();
                       while ( (linefrombug= brBI.readLine()) != null) {
                          x++;
                          String[] tokenBug = linefrombug.split(csvSplitBy);

      				            String[] classes = tokenBug[2].split("\\|");
                          String fixversion = tokenBug[5];
      				            for (Integer i = 0; i < classes.length; i++) {
                            if ( versionclassBugFixes.containsKey(fixversion+","+classes[i]))
                               versionclassBugFixes.put(fixversion+","+classes[i], versionclassBugFixes.get(fixversion+","+classes[i]) + 1);
                            else
                               versionclassBugFixes.put(fixversion+","+classes[i], 1);
                          }
                       }
                    } catch (IOException e) {
                       e.printStackTrace();
                    }

				            while ( (linefrommetrics = brMetrics.readLine()) != null) {

				              String[] tokenMetrics = linefrommetrics.split(csvSplitBy);
                      fileWriter.append(tokenMetrics[0] + "," + tokenMetrics[1] + "," + tokenMetrics[2] + "," +tokenMetrics[3] + "," +tokenMetrics[4] + ",");
                      if ( versionclassBugFixes.containsKey(tokenMetrics[0]+","+tokenMetrics[1]))
                         fileWriter.append(versionclassBugFixes.get(tokenMetrics[0]+","+tokenMetrics[1]) + ",");
                      else
                         fileWriter.append("0,");
                      fileWriter.append(tokenMetrics[6] + "," + tokenMetrics[7] + "," + tokenMetrics[8] + "," +tokenMetrics[9] + "," +
                      tokenMetrics[10] + "," + tokenMetrics[11] + "," + tokenMetrics[12] + "," +tokenMetrics[13] + "," +
                      tokenMetrics[14] + "," + tokenMetrics[15] + "," + tokenMetrics[16] + "," +tokenMetrics[17] + "\n");

				            }
				         } catch (Exception e) {
				            System.out.println("Error in csv writer " + x + "\n");
				            e.printStackTrace();
				         } finally {
				            try {
				               fileWriter.flush();
				               fileWriter.close();
				            } catch (IOException e) {
				               System.out.println("Error while flushing/closing fileWriter !!!");
				               e.printStackTrace();
				            }
				         }
				      } catch (IOException e) {
				         e.printStackTrace();
				      }

           }

      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
