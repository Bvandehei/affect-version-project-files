import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.lang.Math;

import org.json.*;

public class NewRetrievalMethods {
   public static HashMap<String, Double> ProjectAndP;
   public static HashMap<String, Double> ProjectAndJavaFiles;
   public static HashMap<String, String> BugAndFixHash;
   public static HashMap<String, String> FixHashAndBIC;
   public static HashMap<String, LocalDateTime> FixHashAndBICDate;
   public static HashMap<String, ArrayList<LocalDateTime>> FixHashAndBICSDates;
   public static HashMap<String, Integer> FixHashAndBICVersion;
   public static HashMap<String, ArrayList<Integer>> FixHashAndBICSVersions;
   public static HashMap<Integer, String> BugAndKey;
   public static HashMap<Integer, Integer> BugAndAV;
   public static HashMap<Integer, Integer> BugAndCreation;
   public static HashMap<Integer, Integer> BugAndFix;
   public static HashMap<String, String> BugAndFixDate;
   public static HashMap<String, Integer> BugAndRASZZFixVersion;
   public static HashMap<String, Integer> BugAndBSZZFixVersion;
   public static HashMap<Integer, String> releaseNames;
   public static HashMap<Integer, String> releaseID;
   public static ArrayList<LocalDateTime> releases;

  //Adds a release to the arraylist
   public static void addRelease(String strDate) {
      LocalDateTime dateTime = LocalDateTime.parse(strDate);
      releases.add(dateTime);
      return;
   }

   // Finds the release corresponding to a creation or resolution date
   public static Integer findRelease(LocalDateTime dateTime) {
      int i;
      int index = 0;
      for (i = 0; i < releases.size(); i++ ) {
         // if date is after this release, the set to next index
         if (dateTime.isAfter(releases.get(i)))
            index = i + 1;
      }
      //acount for 0 offset
      return index + 1;
   }

   public static void main(String[] args) throws IOException {
      String csvFile = "../RQ1/TopProjects.csv";
      String cvsSplitBy = ",";

     try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
         ProjectAndP = new HashMap<String, Double>();
         String linefromcsv;
         FileWriter fileWriter = null;
         try {
           //Name of CSV for output
			     fileWriter = new FileWriter("BugsAndColdStartP.csv");
           fileWriter.append("Project,Bug ID,IV,OV,FV,P\n");
           while ( (linefromcsv = br.readLine()) != null) {
               Integer totalBugs= 0;
               String[] token = linefromcsv.split(cvsSplitBy);
               String bugfilename = "../BugInfoFiles/Ordered"+ token[0] + "BugInfo.csv";

               //Get Bug Info for each project
               try (BufferedReader brBugs = new BufferedReader(new FileReader(bugfilename))) {
                    String linefrombug;
                    Integer bugOrder = 1;
                    BugAndKey = new HashMap<Integer, String>();
                    BugAndAV = new HashMap<Integer, Integer>();
                    BugAndCreation = new HashMap<Integer, Integer>();
                    BugAndFix = new HashMap<Integer, Integer>();
                    BugAndFixHash = new HashMap<String, String>();
                    brBugs.readLine();
                    while ( (linefrombug = brBugs.readLine()) != null) {
                      String[] tokenBug = linefrombug.split(cvsSplitBy);
                      BugAndKey.put(bugOrder, tokenBug[0]);
                      BugAndAV.put(bugOrder, Integer.parseInt(tokenBug[3]));
                      BugAndCreation.put(bugOrder, Integer.parseInt(tokenBug[4]));
                      BugAndFix.put(bugOrder, Integer.parseInt(tokenBug[5]));
                      BugAndFixHash.put(tokenBug[0], tokenBug[8]);
                      bugOrder++;
                   }
                   totalBugs = BugAndKey.size();
               } catch (IOException e) {
                 e.printStackTrace();
               }
               Double Psum = 0.0;
               Double Ptotal = 0.0;
               for ( int i = 1; i <= totalBugs; i++) {
                  Double TV2FV = 1.0;
                  if ( BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                     TV2FV = (double)(BugAndFix.get(i) - BugAndCreation.get(i));
                  Psum += ((double)(BugAndFix.get(i) - BugAndAV.get(i)) / TV2FV);
                  fileWriter.append(token[0] +"," + BugAndKey.get(i) + "," + BugAndAV.get(i)
                    +"," + BugAndCreation.get(i) + "," + BugAndFix.get(i) + ","
                    + ((double)(BugAndFix.get(i) - BugAndAV.get(i)) / TV2FV) + "\n");
                  Ptotal = (double)i;
               }
               ProjectAndP.put(token[0], Psum/Ptotal);
               //System.out.println(token[0] +"," +ProjectAndP.get(token[0]).toString());
           }
         }catch (Exception e) {
            System.out.println("Error in csv writer");
            e.printStackTrace();
         } finally {
            try {
               fileWriter.flush();
               fileWriter.close();
            } catch (IOException e) {
               System.out.println("Error while flushing/closing fileWriter !!!");
               e.printStackTrace();
            }
         }
     } catch (IOException e) {
         e.printStackTrace();
     }

     String detailsfilename =  "../RQ1/ProjectDetails.csv";
     ProjectAndJavaFiles = new HashMap<String, Double>();
     try (BufferedReader br = new BufferedReader(new FileReader(detailsfilename))) {
        br.readLine();
        String line;
        while ( (line = br.readLine()) != null) {
           String[] tokenDetails= line.split(cvsSplitBy);
           ProjectAndJavaFiles.put(tokenDetails[0], Double.parseDouble(tokenDetails[5]));
        }
     } catch (IOException e) {
        e.printStackTrace();
     }

     try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
         String linefromcsv, linefromversion, linefrombug, linefromhash;
         Integer totalBugs = 0, totalVersions = 0;
         FileWriter fileWriter = null;
         FileWriter fileWriter2 = null;
         FileWriter fileWriterP = null;
         FileWriter fileWriterMWP = null;
         try {
            //Name of CSV for output
				    fileWriter = new FileWriter("MethodsResults.csv");
				    fileWriter2 = new FileWriter("MethodsStatistics.csv");
				    fileWriterP = new FileWriter("IncrementPValues.csv");
				    fileWriterMWP = new FileWriter("MovingWindowPValues.csv");

            //Header for CSV
            fileWriter.append("Project Key,Bug ID,Bug Order,Version ID,Version Name,Version Index," +
                              "Simple,ProportionColdStart1,ProportionColdStart0.5,ProportionColdStart2,ProportionIncrement1,ProportionIncrement0.5,ProportionIncrement2,ProportionMovingWindow1%," +
                              "ProportionMovingWindow5%,ProportionMovingWindow10%,U-SZZ,B-SZZ,RA-SZZ,"+
                              "ProportionColdStart1+,ProportionColdStart0.5+,ProportionColdStart2+,ProportionIncrement1+,ProportionIncrement0.5+,ProportionIncrement2+,ProportionMovingWindow1%+," +
                              "ProportionMovingWindow5%+,ProportionMovingWindow10%+,U-SZZ+,B-SZZ+,RA-SZZ+,"+
                              "ActualBugginess");
            fileWriter.append("\n");
            fileWriter2.append("Project Key,Method,Percent Java Files,Precision,Recall,F1,MCC,Kappa");
            fileWriter2.append("\n");
            fileWriterP.append("Project,P for this whole project,Cold Start P,P[Version 1],P[Version 2],P[Version 3],P[Version 4],P[Version 5],P[Version 6]...\n");
            fileWriterMWP.append("Project,Window %,P[Bug 1%],P[Bug 2%],P[Bug 3%],P[Bug 4%],P[Bug 5%],P[Bug 6%]...\n");

            while ( (linefromcsv = br.readLine()) != null) {
               String[] token = linefromcsv.split(cvsSplitBy);

               System.out.println(token[0]);

               //First get Bug id and fix commit Hash
               String hashfilename = "../BugInfoFiles/Ordered"+ token[0] + "BugInfo.csv";
               try (BufferedReader brHash = new BufferedReader(new FileReader(hashfilename))) {
                 BugAndFixHash = new HashMap<String, String>();
                 BugAndFixDate = new HashMap<String, String>();
                 brHash.readLine();
                 while ( (linefromhash = brHash.readLine()) != null) {
                    String[] tokenHash = linefromhash.split(cvsSplitBy);
                    BugAndFixHash.put(tokenHash[0], tokenHash[8]);
                    BugAndFixDate.put(tokenHash[0], tokenHash[7].replace(" ", "").replace(":", ""));
                 }
               } catch (IOException e) {
                 e.printStackTrace();
               }
               //Next get fix commit Hash and oldest hash introducer
               FixHashAndBIC = new HashMap<String, String>();
               FixHashAndBICDate = new HashMap<String, LocalDateTime>();
               FixHashAndBICSDates = new HashMap<String, ArrayList<LocalDateTime>>();
               for (int h = 1; h < token.length; h++) {
                  String[] token2 = token[h].split("/");
                  String path =  token2[token2.length - 1].substring(0, token2[token2.length - 1].length() - 4);
                  hashfilename = "../U-SZZ/FixAndIntroducingCommitsFiles/"+token[0]+"_"+path + "_fix_and_introducers_pairs_.json";
                  String command0 = "git clone " + token[h];
                  Process proc0 = Runtime.getRuntime().exec(command0);
                  proc0.waitFor();
                  try (BufferedReader brHash = new BufferedReader(new FileReader(hashfilename))) {
                     StringBuilder sb = new StringBuilder();
                     String result = "";
                     String line = brHash.readLine();
                     while (line != null) {
                       sb.append(line);
                       line = brHash.readLine();
                     }
                     result = sb.toString();
                     JSONArray jarr = new JSONArray(result);
                     for ( int j = 0; j < jarr.length(); j++) {
                        JSONArray pair = jarr.getJSONArray(j);
                        String fix = pair.getString(0);
                        String bic = pair.getString(1);
                        String command = "git --git-dir ./" + path + "/.git " +
                                 "show -s --format=%cd --date=iso-strict " + bic;
                        Process proc = Runtime.getRuntime().exec(command);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                        String date;
                        if((date = reader.readLine()) != null) {
                           String datePart = date.substring(0, 19);
                           LocalDateTime datetime = LocalDateTime.parse(datePart);
                           if ( FixHashAndBICSDates.get(fix) == null) {
                              FixHashAndBICSDates.put(fix, new ArrayList<LocalDateTime>());
                              FixHashAndBICSDates.get(fix).add(datetime);
                           }
                           else
                              FixHashAndBICSDates.get(fix).add(datetime);
                           if ( FixHashAndBIC.get(fix) == null || datetime.isBefore(FixHashAndBICDate.get(fix))) {
                              FixHashAndBIC.put(fix, bic);
                              FixHashAndBICDate.put(fix, datetime);
                           }
                        }
                       proc.waitFor();
                     }
                  } catch (IOException e) {
                    e.printStackTrace();
                  }
                  command0 = "rm -rf " + path;
                  proc0 = Runtime.getRuntime().exec(command0);
                  proc0.waitFor();
               }

               String versionfilename = "../VersionInfo/"+ token[0] + "VersionInfo.csv";
               //Get Versions info for each project
               try (BufferedReader brVersions = new BufferedReader(new FileReader(versionfilename))) {
                 releaseNames = new HashMap<Integer, String>();
                 releaseID = new HashMap<Integer, String>();
                 releases = new ArrayList<LocalDateTime>();
                 brVersions.readLine();
                 while ( (linefromversion = brVersions.readLine()) != null) {
                    String[] tokenVersion = linefromversion.split(cvsSplitBy);
                    releaseID.put(Integer.parseInt(tokenVersion[0]), tokenVersion[1]);
                    releaseNames.put(Integer.parseInt(tokenVersion[0]), tokenVersion[2]);
                    addRelease(tokenVersion[3]);
                 }
                 // order releases by date
                 Collections.sort(releases, new Comparator<LocalDateTime>(){
                    @Override
                    public int compare(LocalDateTime o1, LocalDateTime o2) {
                      return o1.compareTo(o2);
                    }
                 });
                 totalVersions = releaseID.size();
               } catch (IOException e) {
                 e.printStackTrace();
               }

               //Now get version associated with BIC
               ArrayList<String> fixKeys = new ArrayList<String>(FixHashAndBICDate.keySet());
               FixHashAndBICVersion = new HashMap<String, Integer>();
               FixHashAndBICSVersions = new HashMap<String, ArrayList<Integer>>();
               for ( int v = 0; v < fixKeys.size(); v++) {
                  FixHashAndBICVersion.put(fixKeys.get(v), findRelease(FixHashAndBICDate.get(fixKeys.get(v))));
                  FixHashAndBICSVersions.put(fixKeys.get(v), new ArrayList<Integer>());
                  for ( int r = 0; r < FixHashAndBICSDates.get(fixKeys.get(v)).size(); r++) {
                      FixHashAndBICSVersions.get(fixKeys.get(v)).add(findRelease(FixHashAndBICSDates.get(fixKeys.get(v)).get(r)));
                  }
                  Collections.sort(FixHashAndBICSVersions.get(fixKeys.get(v)));
                  //System.out.println(Arrays.toString(FixHashAndBICSVersions.get(fixKeys.get(v)).toArray()));
               }
               //For SZZ B
               String bszzfilename = "../affected-version-project-files/bszzresults/bszz_"+ token[0].toLowerCase() + ".csv";
               try (BufferedReader szzbbr = new BufferedReader(new FileReader(bszzfilename))) {
                 BugAndBSZZFixVersion = new HashMap<String, Integer>();
                 szzbbr.readLine();
                 String linefrombszz = null;
                 while ( (linefrombszz = szzbbr.readLine()) != null) {
                    String[] tokenbszz = linefrombszz.split(cvsSplitBy);
                    if (BugAndFixDate.containsKey(tokenbszz[5]) && BugAndFixDate.get(tokenbszz[5]).equals(tokenbszz[4].replace("T","").replace(":",""))) {
                       LocalDateTime dateTime = LocalDateTime.parse(tokenbszz[3].replace(' ', 'T'));
                       Integer ver = findRelease(dateTime);
                       if (BugAndBSZZFixVersion.containsKey(tokenbszz[5]) == false || BugAndBSZZFixVersion.get(tokenbszz[5]) < ver)
                          BugAndBSZZFixVersion.put(tokenbszz[5], ver);
                    }
                 }
               } catch (IOException e) {
                 e.printStackTrace();
               }
               Boolean rafile = true;
               //For SZZ RA
               String raszzfilename = "../affected-version-project-files/raszzresults/raszz_"+ token[0].toLowerCase() + ".csv";
               try (BufferedReader szzrabr = new BufferedReader(new FileReader(raszzfilename))) {
                 BugAndRASZZFixVersion = new HashMap<String, Integer>();
                 szzrabr.readLine();
                 String linefromraszz = null;
                 while ( (linefromraszz = szzrabr.readLine()) != null) {
                    String[] tokenraszz = linefromraszz.split(cvsSplitBy);
                    if (BugAndFixDate.containsKey(tokenraszz[5]) && BugAndFixDate.get(tokenraszz[5]).equals(tokenraszz[4].replace("T","").replace(":",""))) {
                       LocalDateTime dateTime = LocalDateTime.parse(tokenraszz[3].replace(' ', 'T'));
                       Integer ver = findRelease(dateTime);
                       if (BugAndRASZZFixVersion.containsKey(tokenraszz[5]) == false || BugAndRASZZFixVersion.get(tokenraszz[5]) < ver)
                          BugAndRASZZFixVersion.put(tokenraszz[5], ver);
                    }
                 }
               } catch (IOException e) {
                 System.out.println("No RA file");
                 rafile = false;
               }
               String bugfilename = "../BugInfoFiles/Ordered"+ token[0] + "BugInfo.csv";
               //Get Bug Info for each project
               try (BufferedReader brBugs = new BufferedReader(new FileReader(bugfilename))) {
                  Integer bugOrder = 1;
                  BugAndKey = new HashMap<Integer, String>();
                  BugAndAV = new HashMap<Integer, Integer>();
                  BugAndCreation = new HashMap<Integer, Integer>();
                  BugAndFix = new HashMap<Integer, Integer>();
                  brBugs.readLine();
                  while ( (linefrombug = brBugs.readLine()) != null) {
                     String[] tokenBug = linefrombug.split(cvsSplitBy);
                     BugAndKey.put(bugOrder, tokenBug[0]);
                     BugAndAV.put(bugOrder, Integer.parseInt(tokenBug[3]));
                     BugAndCreation.put(bugOrder, Integer.parseInt(tokenBug[4]));
                     BugAndFix.put(bugOrder, Integer.parseInt(tokenBug[5]));
                     bugOrder++;
                  }
                  totalBugs = BugAndKey.size();
               } catch (IOException e) {
                  e.printStackTrace();
               }

               // Get versions increments
               Integer numVersions = releases.size();
               Double incr = 1.0;
               //if (numVersions > 100)
               //   incr = (double)numVersions/100.0;
               Long sTP = 0L, sTN = 0L, sFP = 0L, sFN = 0L;
               Long piTP = 0L, piTN = 0L, piFP = 0L, piFN = 0L;
               Long pipTP = 0L, pipTN = 0L, pipFP = 0L, pipFN = 0L;
               Long pi0_5TP = 0L, pi0_5TN = 0L, pi0_5FP = 0L, pi0_5FN = 0L;
               Long pi0_5pTP = 0L, pi0_5pTN = 0L, pi0_5pFP = 0L, pi0_5pFN = 0L;
               Long pi2TP = 0L, pi2TN = 0L, pi2FP = 0L, pi2FN = 0L;
               Long pi2pTP = 0L, pi2pTN = 0L, pi2pFP = 0L, pi2pFN = 0L;
               Long pw1TP = 0L, pw1TN = 0L, pw1FP = 0L, pw1FN = 0L;
               Long pw1pTP = 0L, pw1pTN = 0L, pw1pFP = 0L, pw1pFN = 0L;
               Long pw5TP = 0L, pw5TN = 0L, pw5FP = 0L, pw5FN = 0L;
               Long pw5pTP = 0L, pw5pTN = 0L, pw5pFP = 0L, pw5pFN = 0L;
               Long pw10TP = 0L, pw10TN = 0L, pw10FP = 0L, pw10FN = 0L;
               Long pw10pTP = 0L, pw10pTN = 0L, pw10pFP = 0L, pw10pFN = 0L;
               Long csTP = 0L, csTN = 0L, csFP = 0L, csFN = 0L;
               Long cspTP = 0L, cspTN = 0L, cspFP = 0L, cspFN = 0L;
               Long cs0_5TP = 0L, cs0_5TN = 0L, cs0_5FP = 0L, cs0_5FN = 0L;
               Long cs0_5pTP = 0L, cs0_5pTN = 0L, cs0_5pFP = 0L, cs0_5pFN = 0L;
               Long cs2TP = 0L, cs2TN = 0L, cs2FP = 0L, cs2FN = 0L;
               Long cs2pTP = 0L, cs2pTN = 0L, cs2pFP = 0L, cs2pFN = 0L;
               Long HunTP = 0L, HunTN = 0L, HunFP = 0L, HunFN = 0L;
               Long HunpTP = 0L, HunpTN = 0L, HunpFP = 0L, HunpFN = 0L;

               Long raTP = 0L, raTN = 0L, raFP = 0L, raFN = 0L;
               Long rapTP = 0L, rapTN = 0L, rapFP = 0L, rapFN = 0L;
               Long bTP = 0L, bTN = 0L, bFP = 0L, bFN = 0L;
               Long bpTP = 0L, bpTN = 0L, bpFP = 0L, bpFN = 0L;

               Double coldStartP = 0.0;
               // get median P value for cold start
               ArrayList<Double> Ps = new ArrayList<Double>();
               for (Map.Entry<String, Double> entry : ProjectAndP.entrySet()) {
                  if (!entry.getKey().equals(token[0])) {
                     Ps.add(entry.getValue());
                  }
               }
               Collections.sort(Ps);
               coldStartP = Ps.get(49);
               HashMap<Integer, Double> arrP = new HashMap<Integer, Double>();
               //calculate Ps
               Double Psum = 0.0;
               Double Ptotal = 0.0;
               //Double v = incr;
               //Integer ip = 1;
               //Integer j = (int)Math.floor(incr);
               HashMap<Integer, ArrayList<Integer>> bugsFixedInVersionIncr = new HashMap<Integer, ArrayList<Integer>>();
               for (int i = 1; i<releaseID.size(); i++) {
                     ArrayList<Integer> bugsInVersion = new ArrayList<Integer>();
                     for (Map.Entry<Integer, Integer> entry : BugAndFix.entrySet()) {
                        //if the bug was fixed in that version add to P
                        if (entry.getValue()== i) {
                           bugsInVersion.add(entry.getKey());
                           Double TV2FV = 1.0;
                           Integer k = entry.getKey();
                           if ( BugAndFix.get(k) - BugAndCreation.get(k) != 0)
                              TV2FV = (double)(BugAndFix.get(k) - BugAndCreation.get(k));
                           Psum += ((double)(BugAndFix.get(k) - BugAndAV.get(k)) / TV2FV);
                           Ptotal += 1.0;
                        }
		                 }
                     //if (i == j) {
//System.out.println(Arrays.toString(bugsInVersion.toArray()));
                        bugsFixedInVersionIncr.put(i, bugsInVersion);
                        if(Ptotal < 5.0)
                           arrP.put(i,-1.0);
                        else
                           arrP.put(i, Psum/Ptotal);

                        //ip++;
                        //v+=incr;
                        //j = (int)Math.floor(v);
                        //if (v + incr > releaseID.size())
                        //   j = releaseID.size();
                     //}
               }
               fileWriterP.append(token[0]+","+ProjectAndP.get(token[0]).toString()+","+coldStartP.toString()+",");
               for(int i = 1; i <= arrP.size(); i++){
                  if (arrP.get(i) == -1)
                     fileWriterP.append("undefined,");
                  else
                     fileWriterP.append(arrP.get(i).toString()+",");
               }
               arrP.put(0, -1.0);
               fileWriterP.append("\n");
               Double numBugsIn1Percent = (double)BugAndFix.size() / 100.0;
               Double bugsIncr = numBugsIn1Percent;
               String window1 = token[0]+",1%,";
               String window5 = token[0]+",5%,";
               String window10 = token[0]+",10%,";
               for (int versionIncr = 1; versionIncr<= bugsFixedInVersionIncr.size(); versionIncr++){
               for (int b = 0; b < bugsFixedInVersionIncr.get(versionIncr).size(); b++) {
                  Integer i =  bugsFixedInVersionIncr.get(versionIncr).get(b);
                  Double PsumMW = 0.0;
                  Double PtotalMW = 0.0;
                  Integer bugsToGoBack = (int)Math.round(numBugsIn1Percent);
                  for (int mw = i-1; mw >= i-bugsToGoBack && mw>0; mw--){
                     Double TV2FVMW = 1.0;
                     if ( BugAndFix.get(mw) - BugAndCreation.get(mw) != 0)
                        TV2FVMW = (double)(BugAndFix.get(mw) - BugAndCreation.get(mw));
                     PsumMW += ((double)(BugAndFix.get(mw) - BugAndAV.get(mw)) / TV2FVMW);
                     PtotalMW += 1.0;
                  }
                  Double PMW = PsumMW/PtotalMW;
                  if (PtotalMW <bugsToGoBack) {
                     PMW = coldStartP;
                     if((int)Math.floor(bugsIncr) == i){
                        window1+="undefined,";
                     }
                  }
                  else {
                     if((int)Math.floor(bugsIncr) == i){
                        window1+=PMW.toString()+",";
                     }
                  }
                  Double LAVMW1 = (double)BugAndFix.get(i) - PMW;
                  if (BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                     LAVMW1 = (double)BugAndFix.get(i) - (PMW* (double)(BugAndFix.get(i) - BugAndCreation.get(i)));

                  PsumMW = 0.0;
                  PtotalMW = 0.0;
                  bugsToGoBack = (int)Math.round(numBugsIn1Percent*5.0);
                  for (int mw = i-1; mw >= i-bugsToGoBack && mw>0; mw--){
                     Double TV2FVMW = 1.0;
                     if ( BugAndFix.get(mw) - BugAndCreation.get(mw) != 0)
                        TV2FVMW = (double)(BugAndFix.get(mw) - BugAndCreation.get(mw));
                     PsumMW += ((double)(BugAndFix.get(mw) - BugAndAV.get(mw)) / TV2FVMW);
                     PtotalMW += 1.0;
                  }
                  PMW = PsumMW/PtotalMW;
                  if (PtotalMW <bugsToGoBack) {
                     PMW = coldStartP;
                     if((int)Math.floor(bugsIncr) == i){
                        window5+="undefined,";
                     }
                  }
                  else {
                     if((int)Math.floor(bugsIncr) == i){
                        window5+=PMW.toString()+",";
                     }
                  }
                  Double LAVMW5 = (double)BugAndFix.get(i) - PMW;
                  if (BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                     LAVMW5 = (double)BugAndFix.get(i) - (PMW* (double)(BugAndFix.get(i) - BugAndCreation.get(i)));

                  PsumMW = 0.0;
                  PtotalMW = 0.0;
                  bugsToGoBack = (int)Math.round(numBugsIn1Percent*10);
                  for (int mw = i-1; mw >= i-bugsToGoBack && mw>0; mw--){
                     Double TV2FVMW = 1.0;
                     if ( BugAndFix.get(mw) - BugAndCreation.get(mw) != 0)
                        TV2FVMW = (double)(BugAndFix.get(mw) - BugAndCreation.get(mw));
                     PsumMW += ((double)(BugAndFix.get(mw) - BugAndAV.get(mw)) / TV2FVMW);
                     PtotalMW += 1.0;
                  }
                  PMW = PsumMW/PtotalMW;
                  if (PtotalMW <bugsToGoBack) {
                     PMW = coldStartP;
                     if((int)Math.floor(bugsIncr) == i){
                        window10+="undefined,";
                        bugsIncr += numBugsIn1Percent;
                     }
                  }
                  else {
                     if((int)Math.floor(bugsIncr) == i){
                        window10+=PMW.toString()+",";
                        bugsIncr += numBugsIn1Percent;
                     }
                  }
                  Double LAVMW10 = (double)BugAndFix.get(i) - PMW;
                  if (BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                     LAVMW10 = (double)BugAndFix.get(i) - (PMW* (double)(BugAndFix.get(i) - BugAndCreation.get(i)));

                  for (int j = 1 ; j <= BugAndFix.get(i); j++) {
                     String simple, coldStart, proIncr, proIncrPlus, coldStartPlus, coldStart2, coldStart2Plus, coldStart0_5, coldStart0_5Plus,
                            raSZZ, raSZZPlus, bSZZ, bSZZPlus,
                            proIncr0_5, proIncr2, proIncr0_5p, proIncr2p, szz100, szzp100, proMW1, proMW5, proMW10, proMW1p, proMW5p, proMW10p, actual;

                     if ((j >= LAVMW1 || j >= BugAndCreation.get(i)) && j < BugAndFix.get(i))
                        proMW1p = "Yes";
                     else
                        proMW1p = "No";
                     if (j >= LAVMW1  && j < BugAndFix.get(i))
                        proMW1 = "Yes";
                     else
                        proMW1 = "No";

                     if ((j >= LAVMW5 || j >= BugAndCreation.get(i)) && j < BugAndFix.get(i))
                        proMW5p = "Yes";
                     else
                        proMW5p = "No";
                     if (j >= LAVMW5  && j < BugAndFix.get(i))
                        proMW5 = "Yes";
                     else
                        proMW5 = "No";

                     if ((j >= LAVMW10 || j >= BugAndCreation.get(i)) && j < BugAndFix.get(i))
                        proMW10p = "Yes";
                     else
                        proMW10p = "No";
                     if (j >= LAVMW10  && j < BugAndFix.get(i))
                        proMW10 = "Yes";
                     else
                        proMW10 = "No";

                     if (j < BugAndAV.get(i) || j >= BugAndFix.get(i))
                        actual = "No";
                     else
                        actual = "Yes";
                     if (j >= BugAndCreation.get(i) && j < BugAndFix.get(i))
                        simple = "Yes";
                     else
                        simple = "No";

                     Double LAV = (double)BugAndFix.get(i) - coldStartP;
                     if (BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                        LAV = (double)BugAndFix.get(i) - (coldStartP * (double)(BugAndFix.get(i) - BugAndCreation.get(i)));
                     if ((j >= LAV || j >= BugAndCreation.get(i)) && j < BugAndFix.get(i))
                        coldStartPlus = "Yes";
                     else
                        coldStartPlus = "No";
                     if (j >= LAV  && j < BugAndFix.get(i))
                        coldStart = "Yes";
                     else
                        coldStart = "No";

                     LAV = (double)BugAndFix.get(i) - (coldStartP * 0.5);
                     if (BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                        LAV = (double)BugAndFix.get(i) - ((coldStartP * 0.5)* (double)(BugAndFix.get(i) - BugAndCreation.get(i)));
                     if ((j >= LAV || j >= BugAndCreation.get(i)) && j < BugAndFix.get(i))
                        coldStart0_5Plus = "Yes";
                     else
                        coldStart0_5Plus = "No";
                     if (j >= LAV  && j < BugAndFix.get(i))
                        coldStart0_5 = "Yes";
                     else
                        coldStart0_5 = "No";

                     LAV = (double)BugAndFix.get(i) - (coldStartP * 2.0);
                     if (BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                        LAV = (double)BugAndFix.get(i) - ((coldStartP * 2.0) * (double)(BugAndFix.get(i) - BugAndCreation.get(i)));
                     if ((j >= LAV || j >= BugAndCreation.get(i)) && j < BugAndFix.get(i))
                        coldStart2Plus = "Yes";
                     else
                        coldStart2Plus = "No";
                     if (j >= LAV  && j < BugAndFix.get(i))
                        coldStart2 = "Yes";
                     else
                        coldStart2 = "No";

                     Double incrP = arrP.get(versionIncr-1);
                     if (incrP < 0) {
                        incrP=coldStartP;
                     }
                     LAV = (double)BugAndFix.get(i) - (incrP *0.5);
                     if (BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                        LAV = (double)BugAndFix.get(i) - ((incrP* 0.5) * (double)(BugAndFix.get(i) - BugAndCreation.get(i)));
                     if ((j >= LAV || j >= BugAndCreation.get(i)) && j < BugAndFix.get(i))
                        proIncr0_5p = "Yes";
                     else
                        proIncr0_5p = "No";
                     if (j >= LAV  && j < BugAndFix.get(i))
                        proIncr0_5 = "Yes";
                     else
                        proIncr0_5 = "No";

                     LAV = (double)BugAndFix.get(i) - (incrP*2);
                     if (BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                        LAV = (double)BugAndFix.get(i) - ((incrP*2) * (double)(BugAndFix.get(i) - BugAndCreation.get(i)));
                     if ((j >= LAV || j >= BugAndCreation.get(i)) && j < BugAndFix.get(i))
                        proIncr2p = "Yes";
                     else
                        proIncr2p = "No";
                     if (j >= LAV  && j < BugAndFix.get(i))
                        proIncr2 = "Yes";
                     else
                        proIncr2 = "No";

                     LAV = (double)BugAndFix.get(i) - incrP;
                     if (BugAndFix.get(i) - BugAndCreation.get(i) != 0)
                        LAV = (double)BugAndFix.get(i) - (incrP * (double)(BugAndFix.get(i) - BugAndCreation.get(i)));
                     if ((j >= LAV || j >= BugAndCreation.get(i)) && j < BugAndFix.get(i))
                        proIncrPlus = "Yes";
                     else
                        proIncrPlus = "No";
                     if (j >= LAV  && j < BugAndFix.get(i))
                        proIncr = "Yes";
                     else
                        proIncr = "No";

                     Integer IV = 1;
                     if (FixHashAndBICVersion.containsKey(BugAndFixHash.get(BugAndKey.get(i))))
                        IV = FixHashAndBICSVersions.get(BugAndFixHash.get(BugAndKey.get(i))).get(0);
                     if (j >=  IV  && j < BugAndFix.get(i))
                        szz100 = "Yes";
                     else
                        szz100 = "No";

                     IV = 1;
                     if (FixHashAndBICVersion.containsKey(BugAndFixHash.get(BugAndKey.get(i))))
                        IV = FixHashAndBICSVersions.get(BugAndFixHash.get(BugAndKey.get(i))).get(0);
                     if ((j >=  IV || j >= BugAndCreation.get(i))  && j < BugAndFix.get(i))
                        szzp100 = "Yes";
                     else
                        szzp100 = "No";

                     IV = 1;
                     if (BugAndBSZZFixVersion.containsKey(BugAndKey.get(i)))
                        IV = BugAndBSZZFixVersion.get(BugAndKey.get(i));
                     if (j >=  IV  && j < BugAndFix.get(i))
                        bSZZ = "Yes";
                     else
                        bSZZ = "No";
                     if ((j >=  IV || j >= BugAndCreation.get(i))  && j < BugAndFix.get(i))
                        bSZZPlus = "Yes";
                     else
                        bSZZPlus = "No";

                     IV = 1;
                     if (rafile && BugAndRASZZFixVersion.containsKey(BugAndKey.get(i)))
                        IV = BugAndRASZZFixVersion.get(BugAndKey.get(i));
                     if (rafile == false)
                       raSZZ = szz100;
                     else if (j >=  IV  && j < BugAndFix.get(i))
                        raSZZ = "Yes";
                     else
                        raSZZ = "No";
                     if (rafile == false)
                       raSZZPlus = szzp100;
                     else if ((j >=  IV || j >= BugAndCreation.get(i))  && j < BugAndFix.get(i))
                        raSZZPlus = "Yes";
                     else
                        raSZZPlus = "No";

                     if (actual.equals("Yes")) {
                        if (simple.equals("Yes"))
                           sTP++;
                        else
                           sFN++;
                        if (coldStart.equals("Yes"))
                           csTP++;
                        else
                           csFN++;
                        if (coldStartPlus.equals("Yes"))
                           cspTP++;
                        else
                           cspFN++;
                        if (coldStart2.equals("Yes"))
                           cs2TP++;
                        else
                           cs2FN++;
                        if (coldStart2Plus.equals("Yes"))
                           cs2pTP++;
                        else
                           cs2pFN++;
                        if (coldStart0_5.equals("Yes"))
                           cs0_5TP++;
                        else
                           cs0_5FN++;
                        if (coldStart0_5Plus.equals("Yes"))
                           cs0_5pTP++;
                        else
                           cs0_5pFN++;
                        if (proMW1.equals("Yes"))
                           pw1TP++;
                        else
                           pw1FN++;
                        if (proMW1p.equals("Yes"))
                           pw1pTP++;
                        else
                           pw1pFN++;
                        if (proMW5.equals("Yes"))
                           pw5TP++;
                        else
                           pw5FN++;
                        if (proMW5p.equals("Yes"))
                           pw5pTP++;
                        else
                           pw5pFN++;
                        if (proMW10.equals("Yes"))
                           pw10TP++;
                        else
                           pw10FN++;
                        if (proMW10p.equals("Yes"))
                           pw10pTP++;
                        else
                           pw10pFN++;

                        if (proIncr.equals("Yes"))
                           piTP++;
                        else
                           piFN++;
                        if (proIncrPlus.equals("Yes"))
                           pipTP++;
                        else
                           pipFN++;
                        if (proIncr0_5.equals("Yes"))
                           pi0_5TP++;
                        else
                           pi0_5FN++;
                        if (proIncr0_5p.equals("Yes"))
                           pi0_5pTP++;
                        else
                           pi0_5pFN++;
                        if (proIncr2.equals("Yes"))
                           pi2TP++;
                        else
                           pi2FN++;
                        if (proIncr2p.equals("Yes"))
                           pi2pTP++;
                        else
                           pi2pFN++;
                        if (szz100.equals("Yes"))
                           HunTP++;
                        else
                           HunFN++;
                        if (szzp100.equals("Yes"))
                           HunpTP++;
                        else
                           HunpFN++;

                        if (bSZZ.equals("Yes"))
                           bTP++;
                        else
                           bFN++;
                        if (bSZZPlus.equals("Yes"))
                           bpTP++;
                        else
                           bpFN++;
                        if (raSZZ.equals("Yes"))
                           raTP++;
                        else
                           raFN++;
                        if (raSZZPlus.equals("Yes"))
                           rapTP++;
                        else
                           rapFN++;
                     }
                     else {
                        if (simple.equals("Yes"))
                           sFP++;
                        else
                           sTN++;

                        if (coldStart.equals("Yes"))
                           csFP++;
                        else
                           csTN++;
                        if (coldStartPlus.equals("Yes"))
                           cspFP++;
                        else
                           cspTN++;
                        if (coldStart2.equals("Yes"))
                           cs2FP++;
                        else
                           cs2TN++;
                        if (coldStart2Plus.equals("Yes"))
                           cs2pFP++;
                        else
                           cs2pTN++;
                        if (coldStart0_5.equals("Yes"))
                           cs0_5FP++;
                        else
                           cs0_5TN++;
                        if (coldStart0_5Plus.equals("Yes"))
                           cs0_5pFP++;
                        else
                           cs0_5pTN++;
                        if (proMW1.equals("Yes"))
                           pw1FP++;
                        else
                           pw1TN++;
                        if (proMW1.equals("Yes"))
                           pw1pFP++;
                        else
                           pw1pTN++;
                       if (proMW5.equals("Yes"))
                          pw5FP++;
                       else
                          pw5TN++;
                       if (proMW5.equals("Yes"))
                          pw5pFP++;
                       else
                          pw5pTN++;
                       if (proMW10.equals("Yes"))
                          pw10FP++;
                       else
                          pw10TN++;
                       if (proMW10.equals("Yes"))
                          pw10pFP++;
                       else
                          pw10pTN++;

                        if (proIncr.equals("Yes"))
                           piFP++;
                        else
                           piTN++;
                        if (proIncrPlus.equals("Yes"))
                           pipFP++;
                        else
                           pipTN++;
                        if (proIncr0_5.equals("Yes"))
                           pi0_5FP++;
                        else
                           pi0_5TN++;
                        if (proIncr0_5p.equals("Yes"))
                           pi0_5pFP++;
                        else
                           pi0_5pTN++;
                        if (proIncr2.equals("Yes"))
                           pi2FP++;
                        else
                           pi2TN++;
                        if (proIncr2p.equals("Yes"))
                           pi2pFP++;
                        else
                           pi2pTN++;
                        if (szz100.equals("Yes"))
                           HunFP++;
                        else
                           HunTN++;
                        if (szzp100.equals("Yes"))
                           HunpFP++;
                        else
                           HunpTN++;
                        if (bSZZ.equals("Yes"))
                           bFP++;
                        else
                           bTN++;
                        if (bSZZPlus.equals("Yes"))
                           bpFP++;
                        else
                           bpTN++;
                        if (raSZZ.equals("Yes"))
                           raFP++;
                        else
                           raTN++;
                        if (raSZZPlus.equals("Yes"))
                           rapFP++;
                        else
                           rapTN++;
                     }

                     fileWriter.append(token[0] + "," + BugAndKey.get(i) + "," +
                       i.toString() + "," + releaseID.get(j) + "," + releaseNames.get(j) + "," + j
                       + "," + simple + "," + coldStart +  "," + coldStart0_5 + "," + coldStart2 + "," + proIncr + "," + proIncr0_5 + ","
                       + proIncr2 + "," + proMW1 + "," + proMW5 +","+ proMW10 +  "," + szz100 + "," + bSZZ + "," + raSZZ + ","
                       + coldStartPlus +  "," + coldStart0_5Plus + "," + coldStart2Plus + "," + proIncrPlus + "," + proIncr0_5p +","+ proIncr2p
                       + "," + proMW1p + "," + proMW5p + "," + proMW10p +"," + szzp100 +  ","+ bSZZPlus +  "," + raSZZ +  "," + actual + "\n");
                   }
               }
               }
               fileWriterMWP.append(window1+"\n");
               fileWriterMWP.append(window5+"\n");
               fileWriterMWP.append(window10+"\n");

               Double precision = (double)sTP /(double)(sTP + sFP);
               Double recall = (double)sTP /(double)(sTP + sFN);
               Double F1 = (double)(precision * recall *2) / (double)(precision + recall);
               Double MCC = (double)(sTP * sTN - sFP * sFN) / (Math.sqrt((sTP + sFP) * (sFN + sTN) * (sFP + sTN) * (sTP + sFN)));
               Double observed = (double)(sTP + sTN) / (double)(sTP+ sFN + sFP + sTN);
               Double a = (double)((sTP + sFN) * (sTP + sFP)) / (double)(sTP+ sFN + sFP + sTN);
               Double b = (double)((sFP + sTN) * (sFN + sTN)) / (double)(sTP+ sFN + sFP + sTN);
               Double expected = (a + b) / (double)(sTP+ sFN + sFP + sTN);
               Double kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "Simple" + ","  + ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)csTP /(double)(csTP + csFP);
               recall = (double)csTP /(double)(csTP + csFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((csTP * csTN) - (csFP * csFN)) / (Math.sqrt((double)((csTP + csFP)*(csTP + csFN)*(csTN + csFP)*(csTN+csFN))));
               observed = (double)(csTP + csTN) / (double)(csTP+ csFN + csFP + csTN);
               a = (double)((csTP + csFN) * (csTP + csFP)) / (double)(csTP+ csFN + csFP + csTN);
               b = (double)((csFP + csTN) * (csFN + csTN)) / (double)(csTP+ csFN + csFP + csTN);
               expected = (a + b) / (double)(csTP+ csFN + csFP + csTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionColdStart1" + "," + ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)cspTP /(double)(cspTP + cspFP);
               recall = (double)cspTP /(double)(cspTP + cspFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((cspTP * cspTN) - (cspFP * cspFN)) / (Math.sqrt((double)((cspTP + cspFP)*(cspTP + cspFN)*(cspTN + cspFP)*(cspTN+cspFN))));
               observed = (double)(cspTP + cspTN) / (double)(cspTP+ cspFN + cspFP + cspTN);
               a = (double)((cspTP + cspFN) * (cspTP + cspFP)) / (double)(cspTP+ cspFN + cspFP + cspTN);
               b = (double)((cspFP + cspTN) * (cspFN + cspTN)) / (double)(cspTP+ cspFN + cspFP + cspTN);
               expected = (a + b) / (double)(cspTP+ cspFN + cspFP + cspTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionColdStart1+" + "," + ProjectAndJavaFiles.get(token[0]) + ","+ precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)cs0_5TP /(double)(cs0_5TP + cs0_5FP);
               recall = (double)cs0_5TP /(double)(cs0_5TP + cs0_5FN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((cs0_5TP * cs0_5TN) - (cs0_5FP * cs0_5FN)) / (Math.sqrt((double)((cs0_5TP + cs0_5FP)*(cs0_5TP + cs0_5FN)*(cs0_5TN + cs0_5FP)*(cs0_5TN+cs0_5FN))));
               observed = (double)(cs0_5TP + cs0_5TN) / (double)(cs0_5TP+ cs0_5FN + cs0_5FP + cs0_5TN);
               a = (double)((cs0_5TP + cs0_5FN) * (cs0_5TP + cs0_5FP)) / (double)(cs0_5TP+ cs0_5FN + cs0_5FP + cs0_5TN);
               b = (double)((cs0_5FP + cs0_5TN) * (cs0_5FN + cs0_5TN)) / (double)(cs0_5TP+ cs0_5FN + cs0_5FP + cs0_5TN);
               expected = (a + b) / (double)(cs0_5TP+ cs0_5FN + cs0_5FP + cs0_5TN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionColdStart0.5" + "," + ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)cs0_5pTP /(double)(cs0_5pTP + cs0_5pFP);
               recall = (double)cs0_5pTP /(double)(cs0_5pTP + cs0_5pFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((cs0_5pTP * cs0_5pTN) - (cs0_5pFP * cs0_5pFN)) / (Math.sqrt((double)((cs0_5pTP + cs0_5pFP)*(cs0_5pTP + cs0_5pFN)*(cs0_5pTN + cs0_5pFP)*(cs0_5pTN+cs0_5pFN))));
               observed = (double)(cs0_5pTP + cs0_5pTN) / (double)(cs0_5pTP+ cs0_5pFN + cs0_5pFP + cs0_5pTN);
               a = (double)((cs0_5pTP + cs0_5pFN) * (cs0_5pTP + cs0_5pFP)) / (double)(cs0_5pTP+ cs0_5pFN + cs0_5pFP + cs0_5pTN);
               b = (double)((cs0_5pFP + cs0_5pTN) * (cs0_5pFN + cs0_5pTN)) / (double)(cs0_5pTP+ cs0_5pFN + cs0_5pFP + cs0_5pTN);
               expected = (a + b) / (double)(cs0_5pTP+ cs0_5pFN + cs0_5pFP + cs0_5pTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionColdStart0.5+" + "," + ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)cs2TP /(double)(cs2TP + cs2FP);
               recall = (double)cs2TP /(double)(cs2TP + cs2FN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((cs2TP * cs2TN) - (cs2FP * cs2FN)) / (Math.sqrt((double)((cs2TP + cs2FP)*(cs2TP + cs2FN)*(cs2TN + cs2FP)*(cs2TN+cs2FN))));
               observed = (double)(cs2TP + cs2TN) / (double)(cs2TP+ cs2FN + cs2FP + cs2TN);
               a = (double)((cs2TP + cs2FN) * (cs2TP + cs2FP)) / (double)(cs2TP+ cs2FN + cs2FP + cs2TN);
               b = (double)((cs2FP + cs2TN) * (cs2FN + cs2TN)) / (double)(cs2TP+ cs2FN + cs2FP + cs2TN);
               expected = (a + b) / (double)(cs2TP+ cs2FN + cs2FP + cs2TN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionColdStart2" + "," + ProjectAndJavaFiles.get(token[0]) + ","+ precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)cs2pTP /(double)(cs2pTP + cs2pFP);
               recall = (double)cs2pTP /(double)(cs2pTP + cs2pFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((cs2pTP * cs2pTN) - (cs2pFP * cs2pFN)) / (Math.sqrt((double)((cs2pTP + cs2pFP)*(cs2pTP + cs2pFN)*(cs2pTN + cs2pFP)*(cs2pTN+cs2pFN))));
               observed = (double)(cs2pTP + cs2pTN) / (double)(cs2pTP+ cs2pFN + cs2pFP + cs2pTN);
               a = (double)((cs2pTP + cs2pFN) * (cs2pTP + cs2pFP)) / (double)(cs2pTP+ cs2pFN + cs2pFP + cs2pTN);
               b = (double)((cs2pFP + cs2pTN) * (cs2pFN + cs2pTN)) / (double)(cs2pTP+ cs2pFN + cs2pFP + cs2pTN);
               expected = (a + b) / (double)(cs2pTP+ cs2pFN + cs2pFP + cs2pTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionColdStart2+" + "," + ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pw1TP /(double)(pw1TP + pw1FP);
               recall = (double)pw1TP /(double)(pw1TP + pw1FN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pw1TP * pw1TN) - (pw1FP * pw1FN)) / (Math.sqrt((double)((pw1TP + pw1FP)*(pw1TP + pw1FN)*(pw1TN + pw1FP)*(pw1TN+pw1FN))));
               observed = (double)(pw1TP + pw1TN) / (double)(pw1TP+ pw1FN + pw1FP + pw1TN);
               a = (double)((pw1TP + pw1FN) * (pw1TP + pw1FP)) / (double)(pw1TP+ pw1FN + pw1FP + pw1TN);
               b = (double)((pw1FP + pw1TN) * (pw1FN + pw1TN)) / (double)(pw1TP+ pw1FN + pw1FP + pw1TN);
               expected = (a + b) / (double)(pw1TP+ pw1FN + pw1FP + pw1TN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionMovingWindow1%" + "," + ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pw1pTP /(double)(pw1pTP + pw1pFP);
               recall = (double)pw1pTP /(double)(pw1pTP + pw1pFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pw1pTP * pw1pTN) - (pw1pFP * pw1pFN)) / (Math.sqrt((double)((pw1pTP + pw1pFP)*(pw1pTP + pw1pFN)*(pw1pTN + pw1pFP)*(pw1pTN+pw1pFN))));
               observed = (double)(pw1pTP + pw1pTN) / (double)(pw1pTP+ pw1pFN + pw1pFP + pw1pTN);
               a = (double)((pw1pTP + pw1pFN) * (pw1pTP + pw1pFP)) / (double)(pw1pTP+ pw1pFN + pw1pFP + pw1pTN);
               b = (double)((pw1pFP + pw1pTN) * (pw1pFN + pw1pTN)) / (double)(pw1pTP+ pw1pFN + pw1pFP + pw1pTN);
               expected = (a + b) / (double)(pw1pTP+ pw1pFN + pw1pFP + pw1pTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionMovingWindow1%+" + "," + ProjectAndJavaFiles.get(token[0]) + ","+ precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pw5TP /(double)(pw5TP + pw5FP);
               recall = (double)pw5TP /(double)(pw5TP + pw5FN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pw5TP * pw5TN) - (pw5FP * pw5FN)) / (Math.sqrt((double)((pw5TP + pw5FP)*(pw5TP + pw5FN)*(pw5TN + pw5FP)*(pw5TN+pw5FN))));
               observed = (double)(pw5TP + pw5TN) / (double)(pw5TP+ pw5FN + pw5FP + pw5TN);
               a = (double)((pw5TP + pw5FN) * (pw5TP + pw5FP)) / (double)(pw5TP+ pw5FN + pw5FP + pw5TN);
               b = (double)((pw5FP + pw5TN) * (pw5FN + pw5TN)) / (double)(pw5TP+ pw5FN + pw5FP + pw5TN);
               expected = (a + b) / (double)(pw5TP+ pw5FN + pw5FP + pw5TN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionMovingWindow5%" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pw5pTP /(double)(pw5pTP + pw5pFP);
               recall = (double)pw5pTP /(double)(pw5pTP + pw5pFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pw5pTP * pw5pTN) - (pw5pFP * pw5pFN)) / (Math.sqrt((double)((pw5pTP + pw5pFP)*(pw5pTP + pw5pFN)*(pw5pTN + pw5pFP)*(pw5pTN+pw5pFN))));
               observed = (double)(pw5pTP + pw5pTN) / (double)(pw5pTP+ pw5pFN + pw5pFP + pw5pTN);
               a = (double)((pw5pTP + pw5pFN) * (pw5pTP + pw5pFP)) / (double)(pw5pTP+ pw5pFN + pw5pFP + pw5pTN);
               b = (double)((pw5pFP + pw5pTN) * (pw5pFN + pw5pTN)) / (double)(pw5pTP+ pw5pFN + pw5pFP + pw5pTN);
               expected = (a + b) / (double)(pw5pTP+ pw5pFN + pw5pFP + pw5pTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionMovingWindow5%+" + "," + ProjectAndJavaFiles.get(token[0]) + ","+ precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pw10TP /(double)(pw10TP + pw10FP);
               recall = (double)pw10TP /(double)(pw10TP + pw10FN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pw10TP * pw10TN) - (pw10FP * pw10FN)) / (Math.sqrt((double)((pw10TP + pw10FP)*(pw10TP + pw10FN)*(pw10TN + pw10FP)*(pw10TN+pw10FN))));
               observed = (double)(pw10TP + pw10TN) / (double)(pw10TP+ pw10FN + pw10FP + pw10TN);
               a = (double)((pw10TP + pw10FN) * (pw10TP + pw10FP)) / (double)(pw10TP+ pw10FN + pw10FP + pw10TN);
               b = (double)((pw10FP + pw10TN) * (pw10FN + pw10TN)) / (double)(pw10TP+ pw10FN + pw10FP + pw10TN);
               expected = (a + b) / (double)(pw10TP+ pw10FN + pw10FP + pw10TN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionMovingWindow10%" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pw10pTP /(double)(pw10pTP + pw10pFP);
               recall = (double)pw10pTP /(double)(pw10pTP + pw10pFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pw10pTP * pw10pTN) - (pw10pFP * pw10pFN)) / (Math.sqrt((double)((pw10pTP + pw10pFP)*(pw10pTP + pw10pFN)*(pw10pTN + pw10pFP)*(pw10pTN+pw10pFN))));
               observed = (double)(pw10pTP + pw10pTN) / (double)(pw10pTP+ pw10pFN + pw10pFP + pw10pTN);
               a = (double)((pw10pTP + pw10pFN) * (pw10pTP + pw10pFP)) / (double)(pw10pTP+ pw10pFN + pw10pFP + pw10pTN);
               b = (double)((pw10pFP + pw10pTN) * (pw10pFN + pw10pTN)) / (double)(pw10pTP+ pw10pFN + pw10pFP + pw10pTN);
               expected = (a + b) / (double)(pw10pTP+ pw10pFN + pw10pFP + pw10pTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionMovingWindow10%+" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)piTP /(double)(piTP + piFP);
               recall = (double)piTP /(double)(piTP + piFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((piTP * piTN) - (piFP * piFN)) / (Math.sqrt((double)((piTP + piFP)*(piTP + piFN)*(piTN + piFP)*(piTN+piFN))));
               observed = (double)(piTP + piTN) / (double)(piTP+ piFN + piFP + piTN);
               a = (double)((piTP + piFN) * (piTP + piFP)) / (double)(piTP+ piFN + piFP + piTN);
               b = (double)((piFP + piTN) * (piFN + piTN)) / (double)(piTP+ piFN + piFP + piTN);
               expected = (a + b) / (double)(piTP+ piFN + piFP + piTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionIncrement1" + "," + ProjectAndJavaFiles.get(token[0]) + ","+ precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pipTP /(double)(pipTP + pipFP);
               recall = (double)pipTP /(double)(pipTP + pipFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pipTP * pipTN) - (pipFP * pipFN)) / (Math.sqrt((double)((pipTP + pipFP)*(pipTP + pipFN)*(pipTN + pipFP)*(pipTN+pipFN))));
               observed = (double)(pipTP + pipTN) / (double)(pipTP+ pipFN + pipFP + pipTN);
               a = (double)((pipTP + pipFN) * (pipTP + pipFP)) / (double)(pipTP+ pipFN + pipFP + pipTN);
               b = (double)((pipFP + pipTN) * (pipFN + pipTN)) / (double)(pipTP+ pipFN + pipFP + pipTN);
               expected = (a + b) / (double)(pipTP+ pipFN + pipFP + pipTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionIncrement1+" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pi0_5TP /(double)(pi0_5TP + pi0_5FP);
               recall = (double)pi0_5TP /(double)(pi0_5TP + pi0_5FN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pi0_5TP * pi0_5TN) - (pi0_5FP * pi0_5FN)) / (Math.sqrt((double)((pi0_5TP + pi0_5FP)*(pi0_5TP + pi0_5FN)*(pi0_5TN + pi0_5FP)*(pi0_5TN+pi0_5FN))));
               observed = (double)(pi0_5TP + pi0_5TN) / (double)(pi0_5TP+ pi0_5FN + pi0_5FP + pi0_5TN);
               a = (double)((pi0_5TP + pi0_5FN) * (pi0_5TP + pi0_5FP)) / (double)(pi0_5TP+ pi0_5FN + pi0_5FP + pi0_5TN);
               b = (double)((pi0_5FP + pi0_5TN) * (pi0_5FN + pi0_5TN)) / (double)(pi0_5TP+ pi0_5FN + pi0_5FP + pi0_5TN);
               expected = (a + b) / (double)(pi0_5TP+ pi0_5FN + pi0_5FP + pi0_5TN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionIncrement0.5" + "," + ProjectAndJavaFiles.get(token[0]) + ","+ precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pi0_5pTP /(double)(pi0_5pTP + pi0_5pFP);
               recall = (double)pi0_5pTP /(double)(pi0_5pTP + pi0_5pFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pi0_5pTP * pi0_5pTN) - (pi0_5pFP * pi0_5pFN)) / (Math.sqrt((double)((pi0_5pTP + pi0_5pFP)*(pi0_5pTP + pi0_5pFN)*(pi0_5pTN + pi0_5pFP)*(pi0_5pTN+pi0_5pFN))));
               observed = (double)(pi0_5pTP + pi0_5pTN) / (double)(pi0_5pTP+ pi0_5pFN + pi0_5pFP + pi0_5pTN);
               a = (double)((pi0_5pTP + pi0_5pFN) * (pi0_5pTP + pi0_5pFP)) / (double)(pi0_5pTP+ pi0_5pFN + pi0_5pFP + pi0_5pTN);
               b = (double)((pi0_5pFP + pi0_5pTN) * (pi0_5pFN + pi0_5pTN)) / (double)(pi0_5pTP+ pi0_5pFN + pi0_5pFP + pi0_5pTN);
               expected = (a + b) / (double)(pi0_5pTP+ pi0_5pFN + pi0_5pFP + pi0_5pTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionIncrement0.5+" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pi2TP /(double)(pi2TP + pi2FP);
               recall = (double)pi2TP /(double)(pi2TP + pi2FN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pi2TP * pi2TN) - (pi2FP * pi2FN)) / (Math.sqrt((double)((pi2TP + pi2FP)*(pi2TP + pi2FN)*(pi2TN + pi2FP)*(pi2TN+pi2FN))));
               observed = (double)(pi2TP + pi2TN) / (double)(pi2TP+ pi2FN + pi2FP + pi2TN);
               a = (double)((pi2TP + pi2FN) * (pi2TP + pi2FP)) / (double)(pi2TP+ pi2FN + pi2FP + pi2TN);
               b = (double)((pi2FP + pi2TN) * (pi2FN + pi2TN)) / (double)(pi2TP+ pi2FN + pi2FP + pi2TN);
               expected = (a + b) / (double)(pi2TP+ pi2FN + pi2FP + pi2TN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionIncrement2" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)pi2pTP /(double)(pi2pTP + pi2pFP);
               recall = (double)pi2pTP /(double)(pi2pTP + pi2pFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((pi2pTP * pi2pTN) - (pi2pFP * pi2pFN)) / (Math.sqrt((double)((pi2pTP + pi2pFP)*(pi2pTP + pi2pFN)*(pi2pTN + pi2pFP)*(pi2pTN+pi2pFN))));
               observed = (double)(pi2pTP + pi2pTN) / (double)(pi2pTP+ pi2pFN + pi2pFP + pi2pTN);
               a = (double)((pi2pTP + pi2pFN) * (pi2pTP + pi2pFP)) / (double)(pi2pTP+ pi2pFN + pi2pFP + pi2pTN);
               b = (double)((pi2pFP + pi2pTN) * (pi2pFN + pi2pTN)) / (double)(pi2pTP+ pi2pFN + pi2pFP + pi2pTN);
               expected = (a + b) / (double)(pi2pTP+ pi2pFN + pi2pFP + pi2pTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "ProportionIncrement2+" + "," + ProjectAndJavaFiles.get(token[0]) + ","+ precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)HunTP /(double)(HunTP + HunFP);
               recall = (double)HunTP /(double)(HunTP + HunFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((HunTP * HunTN) - (HunFP * HunFN)) / (Math.sqrt((double)((HunTP + HunFP)*(HunTP + HunFN)*(HunTN + HunFP)*(HunTN+HunFN))));
               observed = (double)(HunTP + HunTN) / (double)(HunTP+ HunFN + HunFP + HunTN);
               a = (double)((HunTP + HunFN) * (HunTP + HunFP)) / (double)(HunTP+ HunFN + HunFP + HunTN);
               b = (double)((HunFP + HunTN) * (HunFN + HunTN)) / (double)(HunTP+ HunFN + HunFP + HunTN);
               expected = (a + b) / (double)(HunTP+ HunFN + HunFP + HunTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "U-SZZ" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)HunpTP /(double)(HunpTP + HunpFP);
               recall = (double)HunpTP /(double)(HunpTP + HunpFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((HunpTP * HunpTN) - (HunpFP * HunpFN)) / (Math.sqrt((double)((HunpTP + HunpFP)*(HunpTP + HunpFN)*(HunpTN + HunpFP)*(HunpTN+HunpFN))));
               observed = (double)(HunpTP + HunpTN) / (double)(HunpTP+ HunpFN + HunpFP + HunpTN);
               a = (double)((HunpTP + HunpFN) * (HunpTP + HunpFP)) / (double)(HunpTP+ HunpFN + HunpFP + HunpTN);
               b = (double)((HunpFP + HunpTN) * (HunpFN + HunpTN)) / (double)(HunpTP+ HunpFN + HunpFP + HunpTN);
               expected = (a + b) / (double)(HunpTP+ HunpFN + HunpFP + HunpTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "U-SZZ+" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)bTP /(double)(bTP + bFP);
               recall = (double)bTP /(double)(bTP + bFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((bTP * bTN) - (bFP * bFN)) / (Math.sqrt((double)((bTP + bFP)*(bTP + bFN)*(bTN + bFP)*(bTN+bFN))));
               observed = (double)(bTP + bTN) / (double)(bTP+ bFN + bFP + bTN);
               a = (double)((bTP + bFN) * (bTP + bFP)) / (double)(bTP+ bFN + bFP + bTN);
               b = (double)((bFP + bTN) * (bFN + bTN)) / (double)(bTP+ bFN + bFP + bTN);
               expected = (a + b) / (double)(bTP+ bFN + bFP + bTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "B-SZZ" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)bpTP /(double)(bpTP + bpFP);
               recall = (double)bpTP /(double)(bpTP + bpFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((bpTP * bpTN) - (bpFP * bpFN)) / (Math.sqrt((double)((bpTP + bpFP)*(bpTP + bpFN)*(bpTN + bpFP)*(bpTN+bpFN))));
               observed = (double)(bpTP + bpTN) / (double)(bpTP+ bpFN + bpFP + bpTN);
               a = (double)((bpTP + bpFN) * (bpTP + bpFP)) / (double)(bpTP+ bpFN + bpFP + bpTN);
               b = (double)((bpFP + bpTN) * (bpFN + bpTN)) / (double)(bpTP+ bpFN + bpFP + bpTN);
               expected = (a + b) / (double)(bpTP+ bpFN + bpFP + bpTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "B-SZZ+" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)raTP /(double)(raTP + raFP);
               recall = (double)raTP /(double)(raTP + raFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((raTP * raTN) - (raFP * raFN)) / (Math.sqrt((double)((raTP + raFP)*(raTP + raFN)*(raTN + raFP)*(raTN+raFN))));
               observed = (double)(raTP + raTN) / (double)(raTP+ raFN + raFP + raTN);
               a = (double)((raTP + raFN) * (raTP + raFP)) / (double)(raTP+ raFN + raFP + raTN);
               b = (double)((raFP + raTN) * (raFN + raTN)) / (double)(raTP+ raFN + raFP + raTN);
               expected = (a + b) / (double)(raTP+ raFN + raFP + raTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "RA-SZZ" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

               precision = (double)rapTP /(double)(rapTP + rapFP);
               recall = (double)rapTP /(double)(rapTP + rapFN);
               F1 = (precision * recall * 2) / (precision + recall);
               MCC = (double)((rapTP * rapTN) - (rapFP * rapFN)) / (Math.sqrt((double)((rapTP + rapFP)*(rapTP + rapFN)*(rapTN + rapFP)*(rapTN+rapFN))));
               observed = (double)(rapTP + rapTN) / (double)(rapTP+ rapFN + rapFP + rapTN);
               a = (double)((rapTP + rapFN) * (rapTP + rapFP)) / (double)(rapTP+ rapFN + rapFP + rapTN);
               b = (double)((rapFP + rapTN) * (rapFN + rapTN)) / (double)(rapTP+ rapFN + rapFP + rapTN);
               expected = (a + b) / (double)(rapTP+ rapFN + rapFP + rapTN);
               kappa = (observed - expected)/(1 - expected);
               fileWriter2.append(token[0] +  "," + "RA-SZZ+" + ","+ ProjectAndJavaFiles.get(token[0]) + "," + precision.toString() + "," + recall.toString()
                  + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");
            }
         } catch (Exception e) {
            System.out.println("Error in csv writer");
            e.printStackTrace();
         } finally {
            try {
               fileWriter.flush();
               fileWriter.close();
               fileWriter2.flush();
               fileWriter2.close();
               fileWriterP.flush();
               fileWriterP.close();
            } catch (IOException e) {
               System.out.println("Error while flushing/closing fileWriter !!!");
               e.printStackTrace();
            }
         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
